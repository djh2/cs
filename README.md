# Computer Science Courses

Documenting my personal progress through an online Computer Science learning track (EdX, Coursera, etc).

Primarily following the path outlined by [Open Source Society University - Computer Science](https://github.com/ossu/computer-science) and [Teach Yourself Computer Science](https://teachyourselfcs.com/).

## Introduction to Computer Science

**Topics covered**:
`computation`
`imperative programming`
`basic data structures and algorithms`
`and more`

Completed | Courses | Duration | Effort | Prerequisites
:--: | :-- | :--: | :--: | :--:
Yes | [6.0001 - Introduction to Computer Science and Programming in Python - MIT](https://verify.edx.org/cert/f705c33cb53c43aaa8516b12fb9b688c)  ([alt](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-0001-introduction-to-computer-science-and-programming-in-python-fall-2016/)) | 9 weeks | 15 hours/week | high school algebra
Yes | [6.0002 - Introduction to Computational Thinking and Data Science - MIT](https://verify.edx.org/cert/c25d3511b8df4e5b846ff6801515a9cd)  ([alt](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-0002-introduction-to-computational-thinking-and-data-science-fall-2016/)) | 9 weeks | 14-16 hours/week | 6.0001

## Core CS

### Core programming
**Topics covered**:
`functional programming`
`design for testing`
`program requirements`
`common design patterns`
`unit testing`
`object-oriented design`
`Java`
`static typing`
`dynamic typing`
`ML-family languages (via Standard ML)`
`Lisp-family languages (via Racket)`
`Ruby`
`and more`

Completed | Courses | Duration | Effort | Prerequisites
:--: | :-- | :--: | :--: | :--:
Yes | [How to Code - Simple Data](https://www.edx.org/course/how-code-simple-data-ubcx-htc1x) | 7 weeks | 8-10 hours/week | none
Yes | [How to Code - Complex Data](https://www.edx.org/course/how-code-complex-data-ubcx-htc2x) | 6 weeks | 8-10 hours/week | How to Code: Simple Data
Yes | [Software Construction - Data Abstraction](https://www.edx.org/course/software-construction-data-abstraction-ubcx-softconst1x) | 6 weeks | 8-10 hours/week | How to Code - Complex Data
Yes | [Programming Languages, Part A](https://www.coursera.org/learn/programming-languages) | 4 weeks | 8-16 hours/week | recommended: Java, C
Yes | [Programming Languages, Part B](https://www.coursera.org/learn/programming-languages-part-b) | 3 weeks | 8-16 hours/week | Programming Languages, Part A
Yes | [Programming Languages, Part C](https://www.coursera.org/learn/programming-languages-part-c) | 3 weeks | 8-16 hours/week | Programming Languages, Part B

#### Readings
- [ ] **Required** to learn about monads, laziness, purity: [Learn You a Haskell for a Great Good!](http://learnyouahaskell.com/)
- [ ] **Required** to learn about logic programming, backtracking, unification: [Learn Prolog Now!](http://lpn.swi-prolog.org/lpnpage.php?pageid=top)

### Core math

- Should be covered by formal coursework in Mechanical/Aerospace Engineering (BS and MS) including: linear algebra, differential equations, probability & statistics, optimization, and many more.
- I will add specific courses on an as-needed basis.

### Core systems

**Topics covered**:
`procedural programming`
`manual memory management`
`boolean algebra`
`gate logic`
`memory`
`computer architecture`
`assembly`
`machine language`
`virtual machines`
`high-level languages`
`compilers`
`operating systems`
`network protocols`
`and more`

Completed | Courses | Duration | Effort | Additional Text / Assignments | Prerequisites
:--: | :-- | :--: | :--: | :--: | :--:
Yes* | [Introduction to Computer Science - CS50](https://www.edx.org/course/introduction-computer-science-harvardx-cs50x#!) ([alt](https://cs50.harvard.edu/)) | 12 weeks | 10-20 hours/week | *After the sections on C, skip to the next course. | introductory programming
Yes* | [Build a Modern Computer from First Principles: From Nand to Tetris](https://www.coursera.org/learn/build-a-computer) ([alt](http://www.nand2tetris.org/)) | 6 weeks | 7-13 hours/week | * Weeks 1-5 complete | C-like programming language
In Progress | [Build a Modern Computer from First Principles: Nand to Tetris Part II ](https://www.coursera.org/learn/nand2tetris2) | 6 weeks | 12-18 hours/week | - | one of [these programming languages](https://user-images.githubusercontent.com/2046800/35426340-f6ce6358-026a-11e8-8bbb-4e95ac36b1d7.png), From Nand to Tetris Part I
No | [Introduction to Computer Networking](https://lagunita.stanford.edu/courses/Engineering/Networking-SP/SelfPaced/about)| 8 weeks | 4–12 hours/week | [Assignment 1](https://github.com/PrincetonUniversity/COS461-Public/tree/master/assignments/assignment1)<br>[Assignment 2](https://www.scs.stanford.edu/10au-cs144/lab/reliable/reliable.html)<br>[Assignment 3](https://nptel.ac.in/content/storage2/courses/106105080/pdf/M2L7.pdf)<br>[Assignment 4](http://www-net.cs.umass.edu/wireshark-labs/Wireshark_TCP_v7.0.pdf) | algebra, probability, basic CS
No | [ops-class.org - Hack the Kernel](https://www.ops-class.org/) | 15 weeks | 6 hours/week | Replace course textbook with [Operating Systems: Three Easy Pieces](http://pages.cs.wisc.edu/~remzi/OSTEP/) | algorithms

### Core theory

**Topics covered**:
`divide and conquer`
`sorting and searching`
`randomized algorithms`
`graph search`
`shortest paths`
`data structures`
`greedy algorithms`
`minimum spanning trees`
`dynamic programming`
`NP-completeness`
`and more`

Completed | Courses | Duration | Effort | Prerequisites
:--: | :-- | :--: | :--: | :--:
In Progress | [Algorithms, Part I](https://www.coursera.org/learn/algorithms-part1/) | 6 weeks | 6-10 hours/week | any programming language, Mathematics for Computer Science
No | [Algorithms, Part II](https://www.coursera.org/learn/algorithms-part2) | 6 weeks | 6-10 hours/week | Part I


### Core applications

**Topics covered**:
`Agile methodology`
`REST`
`software specifications`
`refactoring`
`relational databases`
`transaction processing`
`data modeling`
`neural networks`
`supervised learning`
`unsupervised learning`
`OpenGL`
`raytracing`
`block ciphers`
`authentication`
`public key encryption`
`and more`

Completed | Courses | Duration | Effort | Prerequisites
:--: | :-- | :--: | :--: | :--:
In Progress | [Databases](https://cs186berkeley.net/)| 14 weeks | 8-12 hours/week | some programming, basic CS
No | [Machine Learning](https://www.coursera.org/learn/machine-learning)| 11 weeks | 4-6 hours/week | linear algebra
No | [Computer Graphics](https://www.edx.org/course/computer-graphics-uc-san-diegox-cse167x)| 6 weeks | 12 hours/week | C++ or Java, linear algebra
No | [Cryptography I](https://www.coursera.org/course/crypto)| 6 weeks | 5-7 hours/week | linear algebra, probability
No | [Software Engineering: Introduction](https://www.edx.org/course/software-engineering-introduction-ubcx-softeng1x) | 6 weeks | 8-10 hours/week | Programming Languages, Part C
No | [Software Development Capstone Project](https://www.edx.org/course/software-development-capstone-project-ubcx-softengprjx) | 6-7 weeks | 8-10 hours/week | Software Engineering: Introduction

## Advanced CS

Here, I will pick some "electives" in area(s) of interest.

### Advanced math

**Topics covered**:
`parametric equations`
`polar coordinate systems`
`multivariable integrals`
`multivariable differentials`
`probability theory`
`and more`

- Should be covered by formal coursework in Mechanical/Aerospace Engineering (BS and MS) including: linear algebra, differential equations, probability & statistics, optimization, and many more.

## Extra/Other/Doesn't Fit Elsewhere

### Language-Specific Refresher

- [x] [Complete Python Bootcamp: Go from zero to hero in Python 3](https://www.udemy.com/course/complete-python-bootcamp/)

### Data Science

- [x] [What is Data Science?](https://www.coursera.org/learn/what-is-datascience)
- [x] [Introduction to Data Science in Python](https://www.coursera.org/learn/python-data-analysis)
- [ ] [Applied Plotting, Charting & Data Representation in Python](https://www.coursera.org/learn/python-plotting)
- [ ] [Applied Machine Learning in Python](https://www.coursera.org/learn/python-machine-learning)
