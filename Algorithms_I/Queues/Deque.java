import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    // constant worst-case time = use (doubly-)linked list(s)! n items must use
    // at most 48n + 192 bytes of memory. Additionally, your iterator
    // implementation must support each operation (including construction) in
    // constant worst-case time. Remove loitering.

    private DoubleNode first;
    private DoubleNode last;
    private int n;

    // construct an empty deque
    public Deque() {
        n = 0;
    }

    // linked list implementation
    // private class Node {
    //     Item item;
    //     Node next;
    // }

    // doubly-linked list implementation
    private class DoubleNode {
        Item item;
        DoubleNode next;
        DoubleNode prev;
    }

    private void checkNullInput(Item item) {
        if (item == null) { throw new IllegalArgumentException(); }
    }

    private void checkEmptyDeque() {
        if (isEmpty()) throw new NoSuchElementException();
    }

    // is the deque empty?
    public boolean isEmpty() {
        return (first == null);
    }

    // return the number of items on the deque
    public int size() {
        return n;
    }

    // add the item to the front
    public void addFirst(Item item) {
        checkNullInput(item);
        if (isEmpty()) {
            first = new DoubleNode();
            first.item = item;
            first.next = null;
            first.prev = null;
            last = first;
        }
        else {
            DoubleNode oldFirst = first;
            first = new DoubleNode();
            first.item = item;
            first.next = oldFirst;
            first.prev = null;
            oldFirst.prev = first;
        }
        n++;
    }

    // add the item to the back
    public void addLast(Item item) {
        checkNullInput(item);
        DoubleNode oldLast = last;
        last = new DoubleNode();
        last.item = item;
        last.next = null;
        if (isEmpty()) {
            first = last;
        }
        else {
            oldLast.next = last;
            last.prev = oldLast;
        }
        n++;
    }

    // remove and return the item from the front
    public Item removeFirst() {
        checkEmptyDeque();
        Item out = first.item;
        first = first.next;
        if (first != null) {
            first.prev = null;
        }
        n--;
        return out;
    }

    // remove and return the item from the back
    public Item removeLast() {
        checkEmptyDeque();
        Item out = last.item;
        last = last.prev;
        if (last != null) {
            last.next = null;
        }
        n--;
        // loitering removal
        if (n == 0) {
            last = null;
            first = null;
        }
        return out;
    }

    // return an iterator over items in order from front to back
    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    private class DequeIterator implements Iterator<Item> {

        private DoubleNode current = first;

        public boolean hasNext() {
            return current != null;
        }

        public Item next() {
            if (!hasNext()) { throw new NoSuchElementException(); }
            Item item = current.item;
            current = current.next;
            return item;
        }

        public void remove() { throw new UnsupportedOperationException(); }
    }

    // unit testing (required)
    public static void main(String[] args) {
        Deque<Integer> myDeque = new Deque<>();
        System.out.println(myDeque.isEmpty());
        myDeque.addFirst(9);
        myDeque.addLast(10);
        System.out.println(myDeque.isEmpty());
        System.out.println(myDeque.first.item);
        System.out.println(myDeque.last.item);
        myDeque.removeFirst();
        System.out.println(myDeque.first.item);
        System.out.println(myDeque.size());
        myDeque.addLast(11);
        myDeque.addLast(12);
        System.out.println(myDeque.size());
        myDeque.removeFirst();
        myDeque.addLast(98);
        myDeque.addLast(99);
        System.out.println(myDeque.size());
        System.out.println(myDeque.last);
        System.out.println(myDeque.removeLast());
        System.out.println(myDeque.size());
        System.out.println(myDeque.last);
        System.out.println(myDeque.removeLast());
        System.out.println("Checking iterator() after 10 calls to addFirst():");
        Deque<Integer> d = new Deque<>();
        for (int i = 0; i < 10; i++) {
            d.addFirst(i);
        }
        for (int i : d) {
            System.out.println(i);
        }
    }
}
