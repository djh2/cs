import edu.princeton.cs.algs4.StdIn;

// TODO - bonus: max size of Deque/RandomizedQueue = k
public class Permutation {
    public static void main(String[] args) {
        int k;
        k = Integer.parseInt(args[0]);
        RandomizedQueue<String> a = new RandomizedQueue<>();
        while (!StdIn.isEmpty()) {
            String tmp = StdIn.readString();
            a.enqueue(tmp);
        }
        for (int i = 0; i < k; i++) {
            System.out.println(a.dequeue());
        }
    }
}
