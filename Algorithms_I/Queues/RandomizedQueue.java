import java.util.Iterator;
import java.util.NoSuchElementException;
import edu.princeton.cs.algs4.StdRandom;

public class RandomizedQueue<Item> implements Iterable<Item>  {

    // Iterator.  Each iterator must return the items in uniformly random order.
    // The order of two or more iterators to the same randomized queue must be mutually independent;
    // each iterator must maintain its own random order.

    // sampling with replacement

    private int n;
    private Item[] rqArray;

    // construct an empty randomized queue. resizing array.
    public RandomizedQueue() {
        n = 0;
        rqArray = (Item[]) new Object[1];
    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return size() == 0;
    }

    // return the number of items on the randomized queue
    public int size() {
        return n;
    }

    // add the item
    public void enqueue(Item item) {

        if (item == null) {
            throw new IllegalArgumentException();
        }

        // check if there's space
        if (n == rqArray.length) {
            resizeArray(2* rqArray.length);
        }
        rqArray[n++] = item;
    }

    private void resizeArray(int max) {
        Item[] newArray = (Item[]) new Object[max];
        for (int i = 0; i < n; i++) {
            newArray[i] = rqArray[i];
        }
        rqArray = newArray;
    }

    // remove and return a random item
    public Item dequeue() {

        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        int index = StdRandom.uniform(n);
        Item removeMe = rqArray[index];

        // cannot shift elements that are right of index, not constant time.
        // instead, swap last element with the removed one
        rqArray[index] = rqArray[n-1];
        rqArray[n-1] = null;
        n--;
        if (n > 0 && n == rqArray.length/4) {
            resizeArray(rqArray.length/2);
        }
        return removeMe;
    }

    // return a random item (but do not remove it)
    public Item sample() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        int sampleIndex = StdRandom.uniform(n);
        return this.rqArray[sampleIndex];
    }

    public Iterator<Item> iterator() {
        return new RQIterator();
    }

    private class RQIterator implements Iterator<Item> {

        // private final RandomizedQueue<Item> temp;
        private final Item[] tempArray;
        private int tempN;

        RQIterator() {
            // two options:
            //     - copy array OR
            //     - create new RandomizedQueue (gave performance warning)
            tempN = n;
            int j = tempN;
            tempArray = (Item[]) new Object[n];
            for (int i = 0; i < j; i++) {
                tempArray[i] = rqArray[i];
            }
            StdRandom.shuffle(tempArray);
        }

        public boolean hasNext() {
            // return !temp.isEmpty();
            return !(tempN == 0);
        }

        public Item next() {
            if (hasNext()) {
                // return temp.dequeue();
                tempN--;
                return tempArray[tempN];
            }
            else {
                throw new NoSuchElementException();
            }
        }

        public void remove() { throw new UnsupportedOperationException(); }
    }

    // unit testing (required)
    public static void main(String[] args) {

        // test empties
        RandomizedQueue<Integer> myRQ = new RandomizedQueue<>();
        myRQ.enqueue(99);
        myRQ.enqueue(1);
        System.out.println(myRQ.sample());
        System.out.println(myRQ.size());
        System.out.println(myRQ.dequeue());

        System.out.println("Multiple iterator test.");
        RandomizedQueue<Integer> myRQ2 = new RandomizedQueue<>();
        for (int i = 0; i < 6; i++) {
            myRQ2.enqueue(i);
        }
        for (int x : myRQ2) {
            for (int y : myRQ2) {
                System.out.print(x + "-" + y + " ");
            }
            System.out.println();
        }
    }
}
